# Qualtrics Basics

### Aidan Wilson

Research Consultant, eResearch Analyst
aidan.wilson@acu.edu.au

--- 

## Outline

- Quick intro to Qualtrics
- Building a survey
- Distributing a survey
- Data
- Support

Note:
Here's a brief outline of this webinar. In each of these sections I'll introduce the topic and describe the sorts of functionality that Qualtrics has, before switching over to Qualtrics to demonstrate.
---

## Qualtrics introduction

- Survey platform
- Fairly intuitive interface
- Quick to build surveys and get them out
- More advanced features when you need them
- Log in using your ACU credentials at [acu.qualtrics.com](https://acu.qualtrics.com)

Note:
Qualtrics is a survey platform that allows you to quickly and easily build surveys using an online tool, and that can be distributed either anonymously, or confidentially to a contact list of email addresses. 
Qualtrics is very intuitive to get set up quickly, and more advanced options are there once you start looking.
To access Qualtrics, log in at this URL, using your ACU credentials and I'll get there soon.

---

## Building a survey

- Basic question types
- Display logic
- Piping values
- Blocks
- Survey flow
- Survey options

Note:
So now we're ready to build a survey. Here are the topics I'll cover in this section. I'll go over some of the question types that you're likely to use . I'll cover some basic display logic, that is, determining whether a question will appear based on the response to some other question.
I'll also cover how to pipe data, to, for example, personalise a survey with someone's name.
I'll discuss question Blocks as a means to organise the structure of a survey, and Survey Flow to configure the ways in which a respondent can go through your survey, kind of like display logic but at a broader level and more powerful.
Lastly I'll cover some of the survey options and tools, such as configuring what happens at the end of a survey.

---

## Distributing a Survey

- Anonymous link
- Contact list

Note:
So now that we've built our survey we can go about distributing it to respondents. There are two main ways of doing that in Qualtrics, by generating and distributing an anonymous link, or by configuring a contact list and sending each person on that list a unique invitation link to complete the survey. Which of these methods is going to work for you will depend on your project, and is something you should have in mind right from the outset.
There is an important difference between these two methods, and that boils down to anonymity versus confidentiality. By using an anonymous link, you can make your survey anonymous (necessary but not sufficient condition), but you won't be able to do things like send thank you emails or reminders. 
By inviting a contact list to complete your survey, you can send reminders and thank yous, but you cannot call your project anonymous; it can only be confidential, and your respondents must be informed of that. For more help on this, I suggest you speak to your supervisor and to the Research Ethics team.

---

## Data

- Viewing and filtering data
- Exporting data
- Building and publishing reports

Note:
Now that we have distributed our survey and possibly collected some data, we'll take a look at how you can view that data, how to export it for analysis in other programs, like SPSS or R, and how to build automated reports in Qualtrics that you can publish to a URL so that you can monitor how your data collection is progressing.

---

## Support

- Online Qualtrics documentation
- Qualtrics support site

Note:
That's about it for the demonstration of Qualtrics' features, but before I finish, I'll point out a few places where you can go to get further help and support for Qualtrics.
First and foremost is the online documentation for Qualtrics. There are articles for just about everything with screenshots and examples that help you make use of even the most advanced functions.
There is also a support site where active Qualtrics users help each other with advice and tips.

---

### Thank you
