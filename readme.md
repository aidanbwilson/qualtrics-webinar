# Qualtrics Basics Webinar

### Aidan Wilson
Research Consultant, eResearch Analyst
aidan.wilson@acu.edu.au

Find this presentation [here](https://gitpitch.com/aidanbwilson/qualtrics-webinar?grs=gitlab).
